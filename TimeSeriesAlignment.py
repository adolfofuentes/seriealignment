# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 17:57:47 2020

@author: fmena
"""

import pandas as pd
import matplotlib.pyplot as plt

#%%

df = pd.read_csv('data_serie_cum.csv')

#%%

#Eje X - se puede elejir como uno desee, aquí simplemente enteros entre 0 y el tamaño de la data
X = range(len(df))

#Series de tiempo
data = df.iloc[:,1:]

#Encontrar el indice del primer elemento que no es cero, para cada columna
idx_nocero = data.ne(0).idxmax()

#%%

#loop sobre las columnas para graficar
for col in data.columns:
    
    #Seleccionamos los valores de la serie desde el idx_nocero en adelante
    idx  = idx_nocero[col]
    yvalues = data.loc[idx:,col].values
 
    #Ahora ajustamos el eje X
    if idx==0:  #si es que no es necesario ajustar el eje X
        xvalues = X
    else:
        xvalues = X[:-idx]
    
    plt.plot(xvalues, yvalues)

plt.xlabel('Eje X')
plt.ylabel('Eje Y')
plt.show()


#%%
#EJEMPLO PARA LAS COLUMNAS 330 A 340

for col in data.columns[330:340]:
    
    idx  = idx_nocero[col]
    yvalues = data.loc[idx:,col].values
 
    if idx==0:  
        xvalues = X
    else:
        xvalues = X[:-idx]
    
    plt.plot(xvalues, yvalues)

plt.xlabel('Eje X')
plt.ylabel('Eje Y')
plt.show()


#%%
#O si deseas que empiecen desde cero, como en el ejemplo de Covid,
#restas un uno del indice 

for col in data.columns[330:340]:
    
    idx  = idx_nocero[col] - 1
    yvalues = data.loc[idx:,col].values
 
    if idx== -1:  
        xvalues = X
    else:
        xvalues = X[:-idx]
    
    plt.plot(xvalues, yvalues)

plt.xlabel('Eje X')
plt.ylabel('Eje Y')
plt.show()


#%%

